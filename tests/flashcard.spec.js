import expect from 'expect';
import moment from 'moment';
import Card from '../src/domain/Card';
import Deck, {makeCardsFromVocab} from '../src/domain/Deck';

var vocabs = [
	{
		id: 1,
		hanzi: 'one'
	},
	{
		id: 2,
		hanzi: 'two'
	},
	{
		id: 3,
		hanzi: 'three'
	}
];

// if you get it wrong it goes back to box 1

describe('leitner box', () => {
	it('review process', () => {
		// it should show study item for card one
		var cards = makeCardsFromVocab(vocabs);
		var deck = new Deck(cards);
		var firstCard = deck.getNextCard();
		// expect(firstCard.hanzi).toBe('one');
		
		console.log(firstCard.hanzi, firstCard.nextReview.format(), firstCard.lastAction);
		firstCard.markAsStudied();

		var secondCard = deck.getNextCard();
		console.log(secondCard.hanzi, secondCard.nextReview.format(), secondCard.lastAction);
		secondCard.markAsStudied();

		var thirdCard = deck.getNextCard();
		console.log(thirdCard.hanzi, thirdCard.nextReview.format(), thirdCard.lastAction);
		thirdCard.markAsStudied();

		var fourthCard = deck.getNextCard();
		console.log(fourthCard.hanzi, fourthCard.nextReview.format(), fourthCard.lastAction);
		fourthCard.markCorrect();

		var fifthCard = deck.getNextCard();
		console.log(fifthCard.hanzi, fifthCard.nextReview.format(), fifthCard.lastAction);
		fifthCard.markWrong();

		var sixth = deck.getNextCard();
		console.log(sixth.hanzi, sixth.nextReview.format(), sixth.lastAction);
		sixth.markCorrect();

		var seventhCard = deck.getNextCard();
		console.log(seventhCard.hanzi, seventhCard.nextReview.format(), seventhCard.lastAction);
		seventhCard.markCorrect();

		var eighthCard = deck.getNextCard();
		console.log(eighthCard.hanzi, eighthCard.nextReview.format(), eighthCard.lastAction);
		eighthCard.markCorrect();

		var ninthCard = deck.getNextCard();
		console.log(ninthCard.hanzi, ninthCard.nextReview.format(), ninthCard.lastAction);
		ninthCard.markCorrect();
	});
});



