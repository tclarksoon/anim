import React, {Component} from 'react';
import { Motion, spring } from 'react-motion';
import Ticker from './Ticker';
import Results from './quiz/Results';
import Choice from './quiz/Choice';

var questions = [
  {
    id: 1,
    title: 'What is 1 + 1',
    choices: [
      {
        id: 1,
        title: 'one'
      },
      {
        id: 2,
        title: 'two'
      },
      {
        id: 3,
        title: 'five'
      },
      {
        id: 4,
        title: 'six'
      }
    ],
    correctChoiceId: 2
  },
  {
    id: 2,
    title: 'What is 2 * 2',
    choices: [
      {
        id: 5,
        title: 'four'
      },
      {
        id: 6,
        title: 'three'
      },
      {
        id: 7,
        title: 'one'
      },
      {
        id: 8,
        title: 'two'
      }
    ],
    correctChoiceId: 5
  }
];

export default class Quiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIdx: 0,
      questions
    };
  }
  selectChoice(choiceId) {
    var {questions, activeIdx} = this.state;
    questions[activeIdx].answer = choiceId;

    this.setState({questions});

    setTimeout(() => {
      this.setState({activeIdx: this.state.activeIdx + 1});
    }, 500);
  }
  render() {
    var isOver = this.state.activeIdx == this.state.questions.length;

    if(isOver) {
      return (
        <Results questions={this.state.questions} />
      );
    }

    var question = this.state.questions[this.state.activeIdx];

    var choiceStyle = {
      flex: '1 0 25%',
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: '30px', 
      height: '60px',
      border: '2px solid #E1EEF3'
    };

    var currentQuestion = (
      <div style={{display: 'flex', flexDirection: 'column'}}>
        <div>
          <h4 style={{margin: 0, padding: '20px', background: 'pink', flexBasis: '40px'}}>{question.title}</h4>
        </div>
        <div style={{padding: '20px', display: 'flex', flexWrap: 'wrap'}}>
        {question.choices.map(c => 
          <Choice
            style={choiceStyle} 
            selectChoice={this.selectChoice.bind(this)}
            key={c.id}
            question={question}
            choice={c} />
        )}
        </div>
      </div>
    );

    var item = {id: question.id, component: currentQuestion};

    return (      
      <Ticker item={item}></Ticker>
    );
  }
}



