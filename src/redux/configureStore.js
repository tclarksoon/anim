import { compose, createStore, applyMiddleware } from 'redux';
import reducer from './reducer';
import thunk from 'redux-thunk';
// import {persistStore, autoRehydrate} from 'redux-persist';

import * as storage from 'redux-storage';
import createEngine from 'redux-storage/engines/localStorage';

// configureStore seems to be standard

export default function configureStore(initialData = {}) {
	// const store = compose(autoRehydrate())(createStore)(reducer)
	// persistStore(store, {blacklist: ['routing']});
	// return store;
	

	// const engine = createEngine('hanzigold');
	// const load = storage.createLoader(engine);
	// const storageMiddleware = storage.createMiddleware(engine);

	// const store = compose(
	//     applyMiddleware(
	//         storageMiddleware
	//     )
	// )(createStore)(storage.reducer(reducer));
	
	// load(store)
	//     .then((newState) => console.log('Loaded state:', newState))
	//     .catch(() => console.log('Failed to load previous state'));

	const createStoreWithMiddleware = applyMiddleware(
	  thunk
	)(createStore);

	const store = createStoreWithMiddleware(reducer);

	return store;
}

import characters from '../characters';

export function seedHanzi(store) {
	var defaultValues = {correctCount: 0, wrongCount: 0};
	var hanzi = characters.map(c => Object.assign(c, defaultValues));
	store.dispatch({type: 'IMPORT', hanzi});
}