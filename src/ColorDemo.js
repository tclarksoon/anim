import React, {Component} from 'react';
import { Motion, spring } from 'react-motion';
import {rgbToHex, interpolateColor} from './color';

const colors = {
  close: '#82b5c6',
  open: '#000000'
};

export default class Demo extends Component {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }
  handleMouseDown() {
    this.setState({open: !this.state.open});
  }
  handleTouchStart(e) {
    e.preventDefault();
    this.handleMouseDown();
  }
  render() {
    return (
      <div>
        <button
          onMouseDown={this.handleMouseDown.bind(this)}
          onTouchStart={this.handleTouchStart.bind(this)}>
          Toggle
        </button>

        <Motion defaultStyle={{val: 0}} style={{val: this.state.open ? spring(400) : 0}}>
          {({val}) =>
            // children is a callback which should accept the current value of
            // `endValue`
            <div className="demo0">
              <div className="demo0-block" style={{
                height: '300px',
                width: '300px',
                WebkitTransform: `translate3d(${val}px, 0, 0)`,
                transform: `translate3d(${val}px, 0, 0)`,
                backgroundColor: interpolateColor(val, colors.close, colors.open, 0, 400),
              }} />
            </div>
          }
        </Motion>
      </div>
    );
  }
}
